FROM openjdk:11-jdk-slim

# Build time variables
ARG MTA_USER_HOME=/home/mta
ARG MBT_VERSION=1.1.1
ARG GO_VERSION=1.13.8
ARG NODE_VERSION=v14.16.1
ARG MAVEN_VERSION=3.6.3

ENV PYTHON /usr/bin/python2.7
ENV M2_HOME=/opt/maven/apache-maven-${MAVEN_VERSION}
ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV NODE_HOME=/opt/nodejs

# Download required env tools
RUN apt-get update && \
    apt-get install --yes --no-install-recommends curl git wget gnupg2 zip unzip build-essential vim && \

    # Change security level as the SAP npm repo doesnt support buster new security upgrade
    # the default configuration for OpenSSL in Buster explicitly requires using more secure ciphers and protocols,
    # and the server running at http://npm.sap.com/ is running software configured to only provide insecure, older ciphers.
    # This causes SSL connections using OpenSSL from a Buster based installation to fail
    # Should be remove once SAP npm repo will patch the security level
    # see - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=912759
    sed -i -E 's/(CipherString\s*=\s*DEFAULT@SECLEVEL=)2/\11/' /etc/ssl/openssl.cnf && \

    # install node
    echo "[INFO] install node ${NODE_VERSION} " && \
    mkdir -p ${NODE_HOME} && \
    curl --fail --silent --output - "https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-x64.tar.gz" \
     | tar -xzv -f - -C "${NODE_HOME}" && \
    ln -s "${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin/node" /usr/local/bin/node && \
    ln -s "${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin/npm" /usr/local/bin/npm && \
    ln -s "${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin/npx" /usr/local/bin/npx && \
    echo "[INFO] npm -version" && \
	npm -version && \
	echo "[INFO] node --version" && \
	node --version && \
	echo "[INFO] npm install grunt-cli & ui5 & cds-dk" && \
	npm install --prefix /usr/local/ -g grunt-cli && \
    npm install --prefix /usr/local/ --global @ui5/cli  && \
    npm i --prefix /usr/local/ -g @sap/cds-dk && \
    #ln -s "${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin/cds" /usr/local/bin/cds && \
    # config NPM
    #echo "[INFO] npm config set @sap:registry" && \
    #npm config set @sap:registry https://npm.sap.com --global && \
    
	# ...first add the Cloud Foundry Foundation public key and package repository to your system
	echo "[INFO] cf-cli " && \
	wget -q -O - https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | apt-key add -  && \
	echo "deb https://packages.cloudfoundry.org/debian stable main" | tee /etc/apt/sources.list.d/cloudfoundry-cli.list  && \
	# ...then, update your local package index, then finally install the cf CLI
	apt-get update  && \
	apt-get install --yes --no-install-recommends cf-cli  && \
	
	# install SAP plugin
    echo "[INFO] install cf-cli sap plugin " && \
	cf add-plugin-repo CF-Community https://plugins.cloudfoundry.org  && \
	cf install-plugin multiapps  -f && \

	#   installing Golang
	echo "[INFO] install golang" && \
    curl -O https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz && tar -xvf go${GO_VERSION}.linux-amd64.tar.gz && \
    mv go /usr/local && \
    mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH" && \
    mkdir -p ${GOPATH}/src ${GOPATH}/bin && \

    # update maven home
     M2_BASE="$(dirname ${M2_HOME})" && \
    mkdir -p "${M2_BASE}" && \
    echo "[INFO] install maven ${MAVEN_VERSION}" && \
	curl --fail --silent --output - "https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz" \
    | tar -xzvf - -C "${M2_BASE}" && \
     ln -s "${M2_HOME}/bin/mvn" /usr/local/bin/mvn && \
     chmod --recursive a+w "${M2_HOME}"/conf/* && \

     # Download MBT
     echo "[INFO] install mbt ${MBT_VERSION}" && \
	 curl -L "https://github.com/SAP/cloud-mta-build-tool/releases/download/v${MBT_VERSION}/cloud-mta-build-tool_${MBT_VERSION}_Linux_amd64.tar.gz" | tar -zx -C /usr/local/bin && \
     chown root:root /usr/local/bin/mbt && \

     # handle users permission
     ### aope036 ### useradd --home-dir "${MTA_USER_HOME}" \
     ### aope036 ###            --create-home \
     ### aope036 ###            --shell /bin/bash \
     ### aope036 ###            --user-group \
     ### aope036 ###            --uid 1000 \
     ### aope036 ###            --comment 'Cloud MTA Build Tool' \
     ### aope036 ###            --password "$(echo weUseMta |openssl passwd -1 -stdin)" mta && \
     ### aope036 ###    # allow anybody to write into the images HOME
     ### aope036 ###    chmod a+w "${MTA_USER_HOME}" && \

    # Install essential build tools and python, required for building db modules
	echo "[INFO] install build-essential & python2.7" && \
    apt-get install --yes --no-install-recommends \
           build-essential \
           python2.7 && \
	echo "[INFO] INSTALL COMPLETED"
    ### aope036 ## apt-get remove --purge --autoremove --yes \
    ### aope036 ##  curl && \
    ### aope036 ## rm -rf /var/lib/apt/lists/*


### aope036 ## ENV PATH=$PATH:./node_modules/.bin HOME=${MTA_USER_HOME}
ENV PATH=$PATH:./node_modules/.bin:${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin
##
##/node-v12.18.3-linux-x64/bin
## /opt/nodejs/node-v12.18.3-linux-x64/bin/cds

WORKDIR /project
